package es.upm.fi.sos.t3.practica;

import java.util.ArrayList;

public class AppGrade {
	private ArrayList<String> course;
	private ArrayList<Double> grades;
	public AppGrade() {
		this.grades = new ArrayList<Double>();
		this.course = new ArrayList<String>();
	}
	public void addGrade(double grade) {
		this.grades.add(grade);
	}
	public void addCourse(String course) {
		this.course.add(course);
	}
	
	public ArrayList<Double> getGrade() {
		return this.grades;
	}
	public ArrayList<String> getCourse() {
		return this.course;
	}
//	public String getName() {
//		return this.name;
//	}
	
	
}
