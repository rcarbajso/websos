
//package es.upm.fi.sos.t3.practica;
//
//import java.rmi.RemoteException;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import es.upm.fi.sos.t3.cliente.UPMCoursesClient;
//import es.upm.fi.sos.t3.cliente.UPMCoursesStub;
//import es.upm.fi.sos.t3.usermanagement.*;
//import es.upm.fi.sos.t3.usermanagement.AddUser;
//import es.upm.fi.sos.t3.usermanagement.AddUserResponse;
//import es.upm.fi.sos.t3.usermanagement.Login;
//import es.upm.fi.sos.t3.usermanagement.LoginResponse;
//import es.upm.fi.sos.t3.usermanagement.Logout;
//import es.upm.fi.sos.t3.usermanagement.RemoveUser;
//import es.upm.fi.sos.t3.usermanagement.RemoveUserResponse;
//import es.upm.fi.sos.t3.usermanagement.ShowCourses;
//import es.upm.fi.sos.t3.usermanagement.ShowCoursesResponse;
//import es.upm.fi.sos.t3.usermanagement.xsd.Course;
//import es.upm.fi.sos.t3.usermanagement.xsd.CourseResponse;
//import es.upm.fi.sos.t3.usermanagement.xsd.Response;
//import es.upm.fi.sos.t3.usermanagement.xsd.*;
//
//
//public class Test {
//
//	public static void main(String[] args) throws RemoteException {
//		// TODO Auto-generated method stub
//		UserManagementWSSkeleton client = new UserManagementWSSkeleton();
////		ShowCourses show = new ShowCourses();
////		Course a = new Course();
////		a.setCourse(3);
////		show.setArgs0(a);
////		ShowCoursesResponse res = client.showCourses(show);
////		CourseResponse c = res.get_return();
////		String[] result = c.getCourseList();
////		for(String elem : result) {
////			System.out.println(elem);
////		}
//		User admin = new User();
//		admin.setName("admin");
//		admin.setPwd("admin");
//		Login adminLogin = new Login();
//		adminLogin.setArgs0(admin);
//		LoginResponse loginAdmin = client.login(adminLogin);
//		System.out.println("LoginResponse: " + loginAdmin.get_return().getResponse());
//		CourseGrade grade = new CourseGrade();
//		grade.setCourse("PROYECTO DE INSTALACION INFORMATICA");
//		grade.setGrade(5.0);
//		AddCourseGrade addCourse = new AddCourseGrade();
//		addCourse.setArgs0(grade);
//		AddCourseGradeResponse addCourseResponse = client.addCourseGrade(addCourse);
//		System.out.println("AddCourseResponse: (true) " + addCourseResponse.get_return().getResponse());
//		grade.setCourse("SISTEMAS ORIENTADOS A SERVICIOS");
//		grade.setGrade(0.0);
//		addCourse.setArgs0(grade);
//		addCourseResponse = client.addCourseGrade(addCourse);
//		System.out.println("AddCourseResponse: (true)" + addCourseResponse.get_return().getResponse());
//		grade.setCourse("SERVICIOS ORIENTADOS A SISTEMAS");
//		grade.setGrade(10.0);
//		addCourse.setArgs0(grade);
//		addCourseResponse = client.addCourseGrade(addCourse);
//		System.out.println("AddCourseResponse: (false)" + addCourseResponse.get_return().getResponse());
//		grade.setCourse("ESPAÑOL PROFESIONAL Y ACADEMICO");
//		grade.setGrade(7.0);
//		addCourse.setArgs0(grade);
//		addCourseResponse = client.addCourseGrade(addCourse);
//		System.out.println("AddCourseResponse: (true)" + addCourseResponse.get_return().getResponse());
//		ShowAllGrades showGrades = new ShowAllGrades();
//		ShowAllGradesResponse showGradesResponse = client.showAllGrades(showGrades);
//		boolean result = showGradesResponse.get_return().getResult();
//		System.out.println("ShowGradesResult: (true)" + result);
//		if(result) {
//			String[] showCourses = showGradesResponse.get_return().getCourses();
//			double[] showGradesResult = showGradesResponse.get_return().getGrades();
//			for(int i = 0; i < showCourses.length; i++) {
//				System.out.println("Asignatura: " + showCourses[i] + " Nota: " + showGradesResult[i]);
//			}
//		}
//		
//		client.logout(new Logout());
//		}
//
//}
