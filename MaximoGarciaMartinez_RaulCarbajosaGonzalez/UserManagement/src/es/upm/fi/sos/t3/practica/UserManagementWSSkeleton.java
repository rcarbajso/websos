
/**
 * UserManagementWSSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package es.upm.fi.sos.t3.practica;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Arrays;
import java.util.Collection;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;

import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion.User;

import java.lang.Object;
import es.upm.fi.sos.t3.cliente.UPMCoursesClient;
import es.upm.fi.sos.t3.cliente.UPMCoursesStub;
import es.upm.fi.sos.t3.usermanagement.AddCourseGradeResponse;
import es.upm.fi.sos.t3.usermanagement.AddUserResponse;
import es.upm.fi.sos.t3.usermanagement.ChangePasswordResponse;
import es.upm.fi.sos.t3.usermanagement.Login;
import es.upm.fi.sos.t3.usermanagement.LoginResponse;
import es.upm.fi.sos.t3.usermanagement.RemoveUser; 
import es.upm.fi.sos.t3.usermanagement.RemoveUserResponse;
import es.upm.fi.sos.t3.usermanagement.ShowAllGrades;
import es.upm.fi.sos.t3.usermanagement.ShowAllGradesResponse;
import es.upm.fi.sos.t3.usermanagement.ShowCoursesResponse;
import es.upm.fi.sos.t3.usermanagement.xsd.CourseResponse;
import es.upm.fi.sos.t3.usermanagement.xsd.GradesResponse;
import es.upm.fi.sos.t3.usermanagement.xsd.PasswordPair;
import es.upm.fi.sos.t3.usermanagement.xsd.Response;

/**
 *  UserManagementWSSkeleton java skeleton for the axisService
 */
public class UserManagementWSSkeleton{

	private ArrayList<String> ids = new ArrayList<String>();
	private static HashMap<String,AppUser> users = new HashMap<String,AppUser>();
	private static HashMap<String,AppGrade> grades = new HashMap<String,AppGrade>();
	private boolean isLogged;
	private AppUser currentUser;
	private String currentSession;
	public UserManagementWSSkeleton() {
		this.currentSession = MessageContext.getCurrentMessageContext().getServiceGroupContextId();
		if(UserManagementWSSkeleton.users.isEmpty()) {
			AppUser admin = new AppUser("admin","admin",true);
			UserManagementWSSkeleton.users.put("admin", admin);
		}
		this.isLogged = false;
		this.currentUser = null;
	}


	/**
	 * Auto generated method signature
	 * 
	 * @param changePassword 
	 * @return changePasswordResponse 
	 */

	public es.upm.fi.sos.t3.usermanagement.ChangePasswordResponse changePassword
	(
			es.upm.fi.sos.t3.usermanagement.ChangePassword changePassword
			)
	{
		ChangePasswordResponse response = new ChangePasswordResponse();
		Response ret = new Response();
		if (this.isLogged) {
			AppUser user = this.currentUser;
			PasswordPair pwdPair = changePassword.getArgs0();
			String pwdOld = pwdPair.getOldpwd();
			if(user.getPwd().equals(pwdOld)) {
				user.setPwd(pwdPair.getNewpwd());
				UserManagementWSSkeleton.users.put(user.getName(), user);
				ret.setResponse(true);
				response.set_return(ret);
				return response;
			}

		}
		ret.setResponse(false);
		response.set_return(ret);
		return response;
	}


	/**
	 * Auto generated method signature
	 * 
	 * @param login 
	 * @return loginResponse 
	 */

	public es.upm.fi.sos.t3.usermanagement.LoginResponse login
	(
			es.upm.fi.sos.t3.usermanagement.Login login
			)
	{
		String name = login.getArgs0().getName();
		String pwd = login.getArgs0().getPwd();
		LoginResponse response = new LoginResponse();
		Response ret = new Response();
		boolean check = false;
		if(!UserManagementWSSkeleton.users.containsKey(name)) {
			ret.setResponse(check);
			response.set_return(ret);
			return response;
		}
		if(this.currentUser != null) {
			if(this.currentUser.getName().equals(name)) {
				check = true;
				ret.setResponse(check);
				response.set_return(ret);
				return response;
			}
			else if(UserManagementWSSkeleton.users.containsKey(this.currentUser.getName())) {
				ret.setResponse(check);
				response.set_return(ret);
				return response;
			}
		}
		if(UserManagementWSSkeleton.users.containsKey(name) && pwd.equals(UserManagementWSSkeleton.users.get(name).getPwd())) {
			AppUser user = UserManagementWSSkeleton.users.get(name);
			user.addSesion(this.currentSession);
			UserManagementWSSkeleton.users.put(name, user);
			check = true;
			this.currentUser = user;
			this.isLogged = true;
		}
		ret.setResponse(check);
		response.set_return(ret);
		return response;
	}


	/**
	 * Auto generated method signature
	 * 
	 * @param logout 
	 * @return  
	 */

	public void logout
	(
			es.upm.fi.sos.t3.usermanagement.Logout logout
			)
	{
		if (this.isLogged) {
			this.ids.remove(this.currentSession);
			this.currentUser.closeSesion(currentSession);
			UserManagementWSSkeleton.users.put(this.currentUser.getName(), this.currentUser);
			this.currentUser = null;
			this.isLogged = false;
		}

	}


	/**
	 * Auto generated method signature
	 * 
	 * @param showCourses 
	 * @return showCoursesResponse 
	 * @throws RemoteException 
	 */

	public es.upm.fi.sos.t3.usermanagement.ShowCoursesResponse showCourses
	(
			es.upm.fi.sos.t3.usermanagement.ShowCourses showCourses
			) throws RemoteException
	{
		int course = showCourses.getArgs0().getCourse();
		ShowCoursesResponse res = new ShowCoursesResponse();
		CourseResponse ret = new CourseResponse();
		boolean check = false;
		String[] subjects = null;
		if(this.isLogged && course < 5 && course > 0 ) {
			UPMCoursesClient client = new UPMCoursesClient();
			subjects = client.showCourses(course);
			ret.setCourseList(subjects);
			check = true;
		}
		ret.setResult(check);
		res.set_return(ret);
		return res;		

	}


	/**
	 * Auto generated method signature
	 * 
	 * @param showAllGrades 
	 * @return showAllGradesResponse 
	 */

	public es.upm.fi.sos.t3.usermanagement.ShowAllGradesResponse showAllGrades
	(
			es.upm.fi.sos.t3.usermanagement.ShowAllGrades showAllGrades
			)
	{
		ShowAllGradesResponse showGrades = new ShowAllGradesResponse();
		GradesResponse response = new GradesResponse();
		response.setResult(false);
		String name = this.currentUser.getName();
		if(this.isLogged) {
			if(UserManagementWSSkeleton.grades.containsKey(name)) {
				AppGrade appGrade = UserManagementWSSkeleton.grades.get(name);
				ArrayList<String> courses = appGrade.getCourse();
				ArrayList<Double> grades = appGrade.getGrade();
				response.setResult(true);
				String[] resCourses = new String[courses.size()];
				for(int i = 0; i < courses.size(); i++) {
					resCourses[i] = courses.get(i);
				}
				double [] resGrades = new double[grades.size()];
				for(int i = 0; i< grades.size(); i++) {
					resGrades[i] = grades.get(i).doubleValue();
				}
				for (int i = 0; i < resGrades.length; i++) {
					for (int j = 1; j<resGrades.length - i; j++) {
						if(resGrades[j]>resGrades[j - 1]) {
							double tempG = resGrades[j -1];
							resGrades[j - 1] = resGrades[j];
							resGrades[j] = tempG;
							String tempC = resCourses[j - 1];
							resCourses[j - 1] = resCourses[j];
							resCourses[j] = tempC;
						}
					}
				}
				response.setCourses(resCourses);
				response.setGrades(resGrades);
			}
		}
		showGrades.set_return(response);
		return showGrades;
	}


	/**
	 * Auto generated method signature
	 * 
	 * @param addUser 
	 * @return addUserResponse 
	 */

	public es.upm.fi.sos.t3.usermanagement.AddUserResponse addUser
	(
			es.upm.fi.sos.t3.usermanagement.AddUser addUser
			)
	{
		Response response = new Response();
		AddUserResponse addUserResponse = new AddUserResponse();
		
		if (this.currentUser == null || !this.currentUser.getSuper() 
				|| UserManagementWSSkeleton.users.containsKey(addUser.getArgs0().getName()))
		{
			response.setResponse(false);
			addUserResponse.set_return(response);
			return addUserResponse;
		}
		String name = addUser.getArgs0().getName();
		String pwd = addUser.getArgs0().getPwd();
		AppUser user = new AppUser(name,pwd,false);
		UserManagementWSSkeleton.users.put(name, user);
		response.setResponse(true);
		addUserResponse.set_return(response);
		return addUserResponse;

	}


	/**
	 * Auto generated method signature
	 * 
	 * @param removeUser 
	 * @return removeUserResponse 
	 */

	public es.upm.fi.sos.t3.usermanagement.RemoveUserResponse removeUser
	(
			es.upm.fi.sos.t3.usermanagement.RemoveUser removeUser
			)
	{
		RemoveUserResponse res = new RemoveUserResponse();
		Response ret = new Response();
		String name = removeUser.getArgs0().getUsername();
		System.out.println("Name: " + name + " Contains: " + UserManagementWSSkeleton.users.containsKey(name));
		if(this.currentUser == null || !UserManagementWSSkeleton.users.containsKey(name) || name.equals("admin")) {
			System.out.println("No existe");
			ret.setResponse(false);
			res.set_return(ret);
			return res;                	 
		}
		if (!this.currentUser.getSuper()) {
			if(this.currentUser.getName().equals(name)) {
				this.currentUser = UserManagementWSSkeleton.users.get(name);
				if(this.currentUser.getSesions().size() > 1) {
					ret.setResponse(false);
					res.set_return(ret);
					return res;
				}
				else {
					boolean check = (UserManagementWSSkeleton.users.remove(name) == null) ? false : true;
					this.currentUser = null;
					ret.setResponse(check);
					res.set_return(ret);
					return res;
				}
			}
			
			else {
				ret.setResponse(false);
				res.set_return(ret);
				return res;
			}
		}
		else {
			boolean check = (UserManagementWSSkeleton.users.remove(name) == null) ? false : true;
			ret.setResponse(check);
			res.set_return(ret);
			return res; 
		}
	}


	/**
	 * Auto generated method signature
	 * 
	 * @param addCourseGrade 
	 * @return addCourseGradeResponse 
	 * @throws RemoteException 
	 */

	public es.upm.fi.sos.t3.usermanagement.AddCourseGradeResponse addCourseGrade
	(
			es.upm.fi.sos.t3.usermanagement.AddCourseGrade addCourseGrade
			) throws RemoteException
	{
		AddCourseGradeResponse gradeResponse = new AddCourseGradeResponse();
		Response ret = new Response();
		ret.setResponse(false);
		String course = addCourseGrade.getArgs0().getCourse();
		double grade = addCourseGrade.getArgs0().getGrade();
		if(grade < 0 || grade > 10) {
			gradeResponse.set_return(ret);
			return gradeResponse;
		}
		UPMCoursesClient client = new UPMCoursesClient();
		if (  this.isLogged && client.checkCourse(course)) {
			String name = this.currentUser.getName();
			AppGrade appGrade;
			if(UserManagementWSSkeleton.grades.containsKey(name)) {
				appGrade = UserManagementWSSkeleton.grades.get(name);
			}
			else {
				appGrade = new AppGrade();
			}
			appGrade.addCourse(course);
			appGrade.addGrade(grade);
			UserManagementWSSkeleton.grades.put(name,appGrade);
			ret.setResponse(true);
		}
		gradeResponse.set_return(ret);
		return gradeResponse;
	}

}
