package es.upm.fi.sos.t3.cliente;
import java.rmi.RemoteException;
import java.util.Stack;

import org.apache.axis2.AxisFault;

import es.upm.fi.sos.t3.cliente.UPMCoursesStub.CheckCourse;
import es.upm.fi.sos.t3.cliente.UPMCoursesStub.*;
import es.upm.fi.sos.t3.cliente.UPMCoursesStub.CheckCourseResponse;
import es.upm.fi.sos.t3.cliente.UPMCoursesStub.ShowCourses;
import es.upm.fi.sos.t3.cliente.UPMCoursesStub.ShowCoursesResponse;

public class UPMCoursesClient {
	private UPMCoursesStub stub;
	
	public UPMCoursesClient() throws AxisFault {
		this.stub = new UPMCoursesStub();
	}
	public String[] showCourses(int curso) throws RemoteException {
		ShowCourses showArg = new ShowCourses();
		showArg.setArgs0(curso);
		ShowCoursesResponse showResponse = new ShowCoursesResponse();
		showResponse = this.stub.showCourses(showArg);
		String[] res = showResponse.get_return();
//		for(String a:res) {
//			System.out.println("Resultado: " + a);
//		}
		return res;
	
	}
	public boolean checkCourse(String course) throws RemoteException {
		CheckCourse check = new CheckCourse();
		check.setArgs0(course);
		CheckCourseResponse response = new CheckCourseResponse();
		response = this.stub.checkCourse(check);
		return response.get_return();
	}
//	public static void main(String[] args) throws RemoteException {
//		showCourses(3);
//	}
}
