package es.upm.fi.sos.t3.practica;

import java.util.ArrayList;
//import java.util.LinkedList;

public class AppUser {
	private String pwd;
	private String name;
	private ArrayList<String> sesionIds;
	private boolean isSuper;
	//If you want you can add name as an atribute (=
	public AppUser(String name,String pwd,boolean isSuper) {
		this.name = name;
		this.pwd = pwd;
		this.isSuper = isSuper;
		this.sesionIds = new ArrayList<String>();
	}
	public AppUser() {
		
	}
	public boolean getSuper() {
		return this.isSuper;
	}
	public String getPwd() {
		return this.pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getName() {
		return this.name;
	}
	public void addSesion(String id) {
		this.sesionIds.add(id);
	}
	public void closeSesion(String id) {
		this.sesionIds.remove(id);
	}
	public ArrayList<String> getSesions(){
		return this.sesionIds;
	}
}
