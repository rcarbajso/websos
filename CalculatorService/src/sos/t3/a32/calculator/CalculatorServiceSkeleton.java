
/**
 * CalculatorServiceSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
    package sos.t3.a32.calculator;
    /**
     *  CalculatorServiceSkeleton java skeleton for the axisService
     */
    public class CalculatorServiceSkeleton{
        private int lastResult = 0;
         
        /**
         * Auto generated method signature
         * 
                                     * @param simpleAddition 
             * @return result 
             * @throws ErrorInOperation 
         */
        
                 public sos.t3.a32.calculator.Result add
                  (
                  sos.t3.a32.calculator.SimpleAddition simpleAddition
                  )
            throws ErrorInOperation{
                	 int sum = simpleAddition.getSimpleAddition().getAddend1() + 
                			 simpleAddition.getSimpleAddition().getAddend2();
                	 Result result = new Result();
                	 result.setResult(sum);
                	 lastResult = sum;
                	 return result;
                //TODO : fill this with the necessary business logic
//                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#add");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param arrayAddition 
             * @return result0 
             * @throws ErrorInOperation 
         */
        
                 public sos.t3.a32.calculator.Result addArray
                  (
                  sos.t3.a32.calculator.ArrayAddition arrayAddition
                  )
            throws ErrorInOperation{
                	 int[] addends = arrayAddition.getArrayAddition().getAddend();
                	 int sum = 0;
                	 for(int addend: addends)
                		 sum += addend;
                	 Result result = new Result();
                	 result.setResult(sum);
                	 lastResult = sum;
                	 return result;
                //TODO : fill this with the necessary business logic
//                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#addArray");
        }
     
         
        /**
         * Auto generated method signature
         * 
                                     * @param increment 
             * @return result1 
             * @throws ErrorInOperation 
         */
        
                 public sos.t3.a32.calculator.Result incrementValue
                  (
                  sos.t3.a32.calculator.Increment increment
                  )
            throws ErrorInOperation{
                	 int sum = lastResult + increment.getIncrement();
                	 Result result = new Result();
                	 result.setResult(sum);
                	 lastResult = sum;
                	 return result;
                //TODO : fill this with the necessary business logic
//                throw new  java.lang.UnsupportedOperationException("Please implement " + this.getClass().getName() + "#incrementValue");
        }
     
    }
    