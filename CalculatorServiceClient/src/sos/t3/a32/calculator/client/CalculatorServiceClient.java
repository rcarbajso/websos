package sos.t3.a32.calculator.client;
import org.apache.axis2.AxisFault;

import sos.t3.a32.calculator.client.CalculatorServiceStub.*;

public class CalculatorServiceClient {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		CalculatorServiceStub cs = new CalculatorServiceStub();
		TSimpleAddition twoAddends = new TSimpleAddition();
		twoAddends.setAddend1(5);
		twoAddends.setAddend2(4);
		SimpleAddition simpleAddition = new SimpleAddition();
		simpleAddition.setSimpleAddition(twoAddends);
		System.out.println("5 + 4 = " + cs.add(simpleAddition).getResult());
	
	}

}
